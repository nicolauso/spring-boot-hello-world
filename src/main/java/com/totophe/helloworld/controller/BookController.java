package com.totophe.helloworld.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.totophe.helloworld.entity.Book;
import com.totophe.helloworld.service.BookService;

@RestController
@RequestMapping("/books")
public class BookController {

    private BookService bookService;

   
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    // CREATE

    @PostMapping("/")

    public Book postBook(@RequestBody Book book) {
        return this.bookService.createBook(book);
    }

    // READ
    @GetMapping("")

    public List<Book> getBookList() {
        List<Book> allBooks = this.bookService.getListBooks();
        return allBooks;
    }

    // SHOW BY TITLE

    @GetMapping("/search/{title}")
    public List<Book> getBookByTitle(@PathVariable String title) {
        return this.bookService.getBooksByTitle(title);
    }

    // SHOW BY ID

    @GetMapping("/search-by-id/{bookId}")
    public Optional<Book> getBookById(@PathVariable Long bookId) {
        return this.bookService.getBookById(bookId);
    }

    // UPDATE

    @PutMapping("/{bookId}")
    public Book updateBookById(Book book, Long bookId) {
        Book bookUpdated = bookService.updateBook(book, bookId);
        return bookUpdated;
    }

    // DELETE

    @DeleteMapping("/{bookId}")
    public String deleteBookById(Long bookId) throws Exception {
        bookService.deleteBookById(bookId);
        return "Deleted Successfully";
    }

}
