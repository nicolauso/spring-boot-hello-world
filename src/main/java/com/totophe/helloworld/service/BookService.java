package com.totophe.helloworld.service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.totophe.helloworld.entity.Book;
import com.totophe.helloworld.repository.BookRepository;

import jakarta.persistence.EntityNotFoundException;

@Service
public class BookService {

    private BookRepository bookRepository;

    @Autowired
    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    // CREATE
    public Book createBook(Book newBook) {
        return this.bookRepository.save(newBook);
    }

    // READ
    public List<Book> getListBooks() {
        return this.bookRepository.findAll();
    }

    // SHOW BY TITLE
    // hibernate creates the rigth query to select the book that contains %title%
    public List<Book> getBooksByTitle(String title) {

        return this.bookRepository.findByTitleContainingIgnoreCase(title);
    }

    // SHOW BY ID

    public Optional<Book> getBookById(Long bookId) {
        return this.bookRepository.findById(bookId);
    }

    // UPDATE
    public Book updateBook(@RequestBody Book book, @PathVariable("id") Long bookId) {

        Book bookDB = bookRepository.findById(bookId)
                .get();

        if (Objects.nonNull(book.getTitle())
                && !"".equalsIgnoreCase(
                        book.getTitle())) {
            bookDB.setTitle(
                    book.getTitle());
        }

        if (Objects.nonNull(
                book.getDescription())
                && !"".equalsIgnoreCase(
                        book.getDescription())) {
            bookDB.setDescription(
                    book.getDescription());
        }

        return bookRepository.save(bookDB);

    }

    // DELETE
    public void deleteBookById(Long bookId) throws Exception {
        Optional<Book> optionalBook = this.bookRepository.findById(bookId);
        if (optionalBook.isPresent()) {
            this.bookRepository.deleteById(bookId);

        } else {
            throw new EntityNotFoundException("No movie found with the id " + bookId);
        }

    }

}
