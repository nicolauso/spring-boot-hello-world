package com.totophe.helloworld.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;

// Crée une entité Book qui contient les attributs : 
//id (Long, clé primaire auto incrémentée), title (String, non null, longueur maximale de 100 caractères), description (String, peut être null, est un texte long), available (Boolean, défaut à true)

@Entity
public class Book {

    public Book() {}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long bookId;

    @Column(length = 100, nullable = false, unique = false)
    private String title;

    @Column(columnDefinition = "TEXT", nullable = true, unique = false)
    private String description;

    @Column(columnDefinition = "BOOLEAN DEFAULT true")
    private Boolean available = true;

    @ManyToOne
    // @JoinColumn(name = "authorId")// si je veux donner un nom spécifique à ma
    // clé étrangère
    @JsonIgnoreProperties({ "bookList" })
     private Author author;

    public Boolean isAvailable() {
        return this.available;
    }

    public Boolean getAvailable() {
        return this.available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public Author getAuthor() {
        return this.author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }
 

    public Long getBookId() {
        return this.bookId;
    }

    public Long setBookId(Long bookId) {
        return this.bookId = bookId;
    }

    public String getTitle() {
        return this.title;
    }

    public String setTitle(String title) {
        return this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public String setDescription(String description) {
        return this.description = description;
    }

}
