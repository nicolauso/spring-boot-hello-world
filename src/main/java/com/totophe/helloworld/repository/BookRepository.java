package com.totophe.helloworld.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.totophe.helloworld.entity.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
    // trouver une livre par son titre
    // query method : hibernate génère la requête automatuiquement à partir du nom
    // de la méthode : SELECT * FROM movie WHERE title LIKE %?%
    public List<Book> findByTitleContainingIgnoreCase(String title);

}
